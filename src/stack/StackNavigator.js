
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../screen/splash/splash';

const Stack = createStackNavigator();

const StackNavigator = () => (

    <Stack.Navigator
        initialRouteName='Splash'
        mode='modal'
        screenOptions={{ headerShown: false }} >
        <Stack.Screen name='Splash' component={Splash} />

    </Stack.Navigator>
)
export default StackNavigator;