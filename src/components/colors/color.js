export default{
    DodgerBlue:'#29abe2',
    White:'#fff',
    Black:'#000',
    ForestGreen:'#28a745',
    LimeGreen:'#3cdc78',
    Red:'#ed1b24',
    Orange :'#f7c100',
    Crimson :'#ff1b6b',
    DarkSlateBlue:'#273d68'

}