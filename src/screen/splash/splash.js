import React, { useEffect } from 'react';
import { View, StyleSheet, Dimensions, } from 'react-native';
import SplashAnimate from './component/splashAnimate';
const { width } = Dimensions.get('window');

const Splash = ({navigation}) => {

    // useEffect(() => {
    //     setTimeout(() => {
    //        navigation.push('a');
    //       }, 5000);
    // }, []);

    return (
        <View style={styles.container}>
            <SplashAnimate />
         </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width:'100%'
    },
});
export default Splash;

