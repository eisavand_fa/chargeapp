
import React from 'react';
import {Dimensions, Animated, StyleSheet, View, Easing} from 'react-native';
import color from '../../../components/colors/color';
const {width, height} = Dimensions.get('window');

export default class SplashAnimate extends React.Component {

  constructor(props) {
    super(props)
    this.animatedValue = new Animated.Value(0)
}
componentDidMount(){
  Animated.timing(this.animatedValue, {
    toValue: 1,
    duration: 2000,
    easing: Easing.ease,
    useNativeDriver:true
}).start()
}

render() {
    return (
      <View style={styles.container}>
            <Animated.Image
                source={require('../../../../assets/logo.gif')}
                resizeMode='contain'
                style={{
                  height:4,
                  width: 4,
                  borderRadius :5,
                  backgroundColor:color.White,
                     transform: [
                         {
                             translateX: this.animatedValue.interpolate({
                                 inputRange: [0, 1],
                                 outputRange: [0, 10]
                             })
                         },
                         {
                             translateY: this.animatedValue.interpolate({
                                 inputRange: [0, 1],
                                 outputRange: [0, 50]
                             })
                         },
                         {
                             scaleX: this.animatedValue.interpolate({
                                 inputRange: [0, 1],
                                 outputRange: [1, 110]
                             })
                         },
                         {
                             scaleY: this.animatedValue.interpolate({
                                 inputRange: [0, 1],
                                 outputRange: [1, 212.5]
                             })
                         }
                     ]
                }}
            />
        </View>
    )
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: width,
    backgroundColor:color.DarkSlateBlue,
  },
});
